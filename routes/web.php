<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pds-records', function () {
     return view('pds.index');
})->name('pds-records');

Route::get('/manage-users', function () {
     return view('user.index');
})->name('manage-users');

Route::get('/reports', function () {
     return view('report.index');
})->name('reports');

Route::get('/records', function () {
     return view('record.index');
})->name('records');

// RESOURCE for CRUD
Route::resource('pds-records', 'PdsRecordsController');
Route::resource('manage-users', 'ManageUsersController');

