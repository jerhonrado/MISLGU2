@extends('user.base')

@section('action-content')
<div class="container-fluid">
	<div class="row">
	    <div class="col-12">
	        <div class="card">
	            <!-- .left-right-aside-column-->
	            <div class="contact-page-aside">
	                <!-- .left-aside-column-->
	                <div class="left-aside bg-light-part">
	                    <ul class="list-style-none">
	                        <li class="box-label"><a href="javascript:void(0)">All Users <span>{{count($user)}}</span></a></li>
	                        <li class="divider"></li>
	                        <li><a href="javascript:void(0)">Administrator <span>0</span></a></li>
	                        <li><a href="javascript:void(0)">User <span>0</span></a></li>
	                        <li><a href="javascript:void(0)">Guest <span>0</span></a></li>
	                    </ul>
	                </div>
	                <!-- /.left-aside-column-->
	                <div class="right-aside ">
                    <div class="col-12">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="flash-message" id="flash-message">
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                  @if(Session::has('alert-' . $msg))

                                  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                  @endif
                                @endforeach
                            </div> <!-- end .flash-message -->
                        </div>
                    </div>
                    
                    <div class="col-md-3 pull-right">
                        <a href="{{ route('manage-users.create') }}" class="btn btn-lg waves-effect waves-light btn-block btn-info col-lg-12 col-md-4">Add User</a>
                    </div>
                    <div class="col-md-9">
                    <h4 class="card-title">List of Administrator and Users</h4>
                    <h6 class="card-subtitle">Description <a href="http://www.csc.gov.ph" target="_blank"><i>CSC</i></a></h6>
                    </div>

                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>Email</th>
                                    <!-- <th>Date Registered</th> -->
                                    <th>Action</th>
                                </tr>
                            </thead> 
                            <tbody>
                            @foreach($user as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->role }}</td>
                                <td>{{ $user->email }}</td>
                                <!-- <td>{{ date('F d, Y', strtotime($user->created_at)) }}</td> -->
                                <td>

                                   <!--  <a href="{{ route('manage-users.edit', ['id' => $user->id]) }}" class="btn btn-xs btn-info">Update</a>
                                    <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a> -->

                                <form class="row" method="POST" action="{{ route('manage-users.destroy', ['id' => $user->id]) }}" onsubmit = "return confirm('Are you sure?')">

                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <a href="{{ route('manage-users.edit', ['id' => $user->id]) }}" class="btn btn-xs btn-info">Update</a>

                                    @if ($user->name)
                                    <button type="submit" class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i>
                                    </button>
                                    @endif

                                </form>

                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
	                </div>
	                <!-- /.left-right-aside-column-->
	            </div>
	        </div>
	    </div>
	</div>
</div>
@endsection