@extends('user.base')

@section('action-content')

<div class="container-fluid">
	<div class="row">
	    <div class="col-12">
	        <div class="card">
	            <!-- .left-right-aside-column-->
	            <div class="contact-page-aside">
	                <!-- .left-aside-column-->
	                <div class="left-aside bg-light-part">
	                    <ul class="list-style-none">
	                        <li class="box-label"><a href="javascript:void(0);">All Users <span>0</span></a></li>
	                        <li class="divider"></li>
	                        <li><a href="javascript:void(0)">Administrator <span>0</span></a></li>
	                        <li><a href="javascript:void(0)">User <span>0</span></a></li>
	                        <li><a href="javascript:void(0)">Guest <span>0</span></a></li>
	                    </ul>
	                </div>
	                <!-- /.left-aside-column-->
	                <div class="right-aside ">
                    <div class="col-12">

                    
                    
                    <div class="row">
	                    <div class="col-md-10">
				    		<div class="card-body">
		                        <h4 class="card-title">Update Administrator</h4>
		                        <h6 class="card-subtitle">Allows to control all the functions of the system</h6>
		                        <hr>
		                        <form role="form" method="POST" action="{{ route('manage-users.update' , ['id' => $user->id]) }}">
			            		{{csrf_field()}}

			            		<input type="hidden" name="_method" value="PATCH">
		                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

		                            <div class="form-group row">
		                                <label for="exampleInputuname3" class="col-sm-3 control-label">Name*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-user"></i></div>
	                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" placeholder="Name" required autofocus>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <label for="exampleInputEmail3" class="col-sm-3 control-label">E-mail*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-email"></i></div>
		                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" placeholder="E-mail" required>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <label for="web" class="col-sm-3 control-label">Role*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-settings"></i></div>
		                                        <!-- <input type="text" class="form-control" id="web" placeholder="Role"> -->
		                                        <select class="form-control custom-select" name="role">
	                                                <option value="{{ $user->role }}">-- {{ $user->role }} --</option>
	                                                <option value="Administrator">Administrator</option>
	                                                <option value="Encoder">Encoder</option>
	                                                <option value="User">User</option>
	                                            </select>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <label for="inputPassword4" class="col-sm-3 control-label">Password*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <label for="inputPassword5" class="col-sm-3 control-label">Re Password*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		                                        <input id="password-confirm" type="password" placeholder="Re-type Password" class="form-control" name="password_confirmation" required>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <div class="col-sm-3">
		                                </div>
		                                <div class="col-sm-3">
		                                	<button type="button" class="btn btn-block btn-sm btn-outline-secondary">Cancel</button>
		                                </div>
		                                <div class="col-sm-6">
		                                    <button type="" class="btn btn-block btn-sm btn-info">Update User</button>
		                                </div>
		                            </div>
		                        </form>
		                    </div>
					    </div>
					</div>
                    
                    </div>
	                </div>
	                <!-- /.left-right-aside-column-->
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection