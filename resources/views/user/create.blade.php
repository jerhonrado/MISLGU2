@extends('user.base')

@section('action-content')

<div class="container-fluid">
	<div class="row">
	    <div class="col-12">
	        <div class="card">
	            <!-- .left-right-aside-column-->
	            <div class="contact-page-aside">
	                <!-- .left-aside-column-->
	                <div class="left-aside bg-light-part">
	                    <ul class="list-style-none">
	                        <li class="box-label"><a href="javascript:void(0);">All Users <span>0</span></a></li>
	                        <li class="divider"></li>
	                        <li><a href="javascript:void(0)">Administrator <span>0</span></a></li>
	                        <li><a href="javascript:void(0)">User <span>0</span></a></li>
	                        <li><a href="javascript:void(0)">Guest <span>0</span></a></li>
	                    </ul>
	                </div>
	                <!-- /.left-aside-column-->
	                <div class="right-aside ">
                    <div class="col-12">

                    <div class="row">
                    	<div class="col-md-12">
					    	<div class="flash-message" id="flash-message">
							    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
							      @if(Session::has('alert-' . $msg))

							      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
							      @endif
							    @endforeach
							</div> <!-- end .flash-message -->
					    </div>
                    </div>
                    
                    <div class="row">
	                    <div class="col-md-10">
				    		<div class="card-body">
		                        <h4 class="card-title">Add Administrator</h4>
		                        <h6 class="card-subtitle">Allows to control all the functions of the system</h6>
		                        <hr>
		                        <form method="POST" action="{{ route('manage-users.store') }}">
			            		{{csrf_field()}}

		                            <div class="form-group row">
		                                <label for="exampleInputuname3" class="col-sm-3 control-label">Name*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-user"></i></div>
	                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
		                                    
		                            @if ($errors->has('name'))
	                                    <span class="invalid-feedback">
	                                        <strong>{{ $errors->first('name') }}</strong>
	                                    </span>
	                                @endif       
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <label for="exampleInputEmail3" class="col-sm-3 control-label">E-mail*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-email"></i></div>
		                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail" required>

		                                        @if ($errors->has('email'))
					                                <span class="invalid-feedback">
					                                    <strong>{{ $errors->first('email') }}</strong>
					                                </span>
					                            @endif
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <label for="web" class="col-sm-3 control-label">Role*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-settings"></i></div>
		                                        <!-- <input type="text" class="form-control" id="web" placeholder="Role"> -->
		                                        <select class="form-control custom-select" name="role" required="">
	                                                <option>-- Please select --</option>
	                                                <option value="Administrator">Administrator</option>
	                                                <option value="Encoder">Encoder</option>
	                                                <option value="User">User</option>
	                                            </select>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <label for="inputPassword4" class="col-sm-3 control-label">Password*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>

		                                         @if ($errors->has('password'))
				                                    <span class="invalid-feedback">
				                                        <strong>{{ $errors->first('password') }}</strong>
				                                    </span>
				                                @endif
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <label for="inputPassword5" class="col-sm-3 control-label">Re Password*</label>
		                                <div class="col-sm-9">
		                                    <div class="input-group">
		                                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		                                        <input id="password-confirm" type="password" placeholder="Re-type Password" class="form-control" name="password_confirmation" required>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="form-group row">
		                                <div class="col-sm-3">
		                                </div>
		                                <div class="col-sm-3">
		                                	<button type="button" class="btn btn-block btn-sm btn-outline-secondary">Cancel</button>
		                                </div>
		                                <div class="col-sm-6">
		                                    <button type="" class="btn btn-block btn-sm btn-info">Save User</button>
		                                </div>
		                            </div>
		                        </form>
		                    </div>
					    </div>
					</div>

				   

                    </div>
	                </div>
	                <!-- /.left-right-aside-column-->
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection



	