@extends('pds.base')

@section('action-content')
<div class="container-fluid">
	<div class="row">
	    <div class="col-12">
	        <div class="card"> 
	            <div class="card-body wizard-content">
                    <div class="col-md-3 pull-right">
		                <a href="/pds-records" class="btn btn-block btn-lg btn-info col-lg-12 col-md-4 pull-right">Records</a>
		            </div>
		            <div class="col-md-9">
		                <h4 class="card-title">Add new record</h4>
		                <h6 class="card-subtitle">Description about adding data <a href="http://www.csc.gov.ph" target="_blank"><i>CSC</i></a></h6>
	                </div>
	                <form action="{{ route('pds-records.store') }}" method="POST" class="tab-wizard wizard-circle">
            		{{csrf_field()}}
	                    
	                    <!-- Step 1 -->
	                    <h6>Personal & Background<br>Information</h6>
	                  		@include('pds.page-1')
	                    <!-- Step 2 -->
	                    <h6>Eligibility &<br>Work Experience</h6>
	                  		@include('pds.page-2')
	                    	
	                    <!-- Step 3 -->
	                    <h6>Voluntary Work &<br>Learning Development</h6>
	                  		@include('pds.page-3')
	                    
	                    <!-- Step 4 -->
	                    <h6>References</h6>
	                  		@include('pds.page-4')
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection