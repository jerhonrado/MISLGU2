<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="{{asset('/assets/images/users/user-1.png')}}" alt="user" /> 
                     <!-- this is blinking heartbit-->
                    <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
            </div>
            <!-- User profile text-->
            <div class="profile-text"> 
                    <h5>{{ Auth::user()->name }}</h5>
                    <a href="" class="" data-toggle="tooltip" title="Profile"><i class="mdi mdi-account-card-details"></i></a>
                    <a href="" class="" data-toggle="tooltip" title="Settings"><i class="mdi mdi-settings"></i></a>
                    <a href="{{ route('logout') }}" title="Logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
            </div>
        </div> 
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-small-cap">HR Management v1.0</li>

                <li class="{{ Request::is('home') ? 'active' : null }}">
                    <a class="waves-effect waves-dark" href="\home" aria-expanded="false">
                        <i class="mdi mdi-chart-timeline"></i><span class="hide-menu">Dashboard</span>
                    </a>
                </li>

                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);">
                        <i class="mdi mdi-book-multiple"></i><span class="hide-menu">201 SALN IPCR Files</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="javascript:void(0);">Add Record</a></li>
                        <li><a href="javascript:void(0);">Upload 201 File</a></li>
                        <li><a href="javascript:void(0);">Upload IPCR File</a></li>
                        <li><a href="javascript:void(0);">View All Records</a></li>
                    </ul>
                </li>

                <li class="{{ Request::is('pds-records') ? 'active' : null }}">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);">
                        <i class="mdi mdi-file"></i><span class="hide-menu">Personal Data Sheet</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('pds-records/create')}}">Add Record</a></li>
                        <li><a href="javascript:void(0);">Upload PDS File</a></li>
                        <li><a href="{{url('pds-records')}}">View All Records</a></li>
                    </ul>
                </li>

                <li class="{{ Request::is('records') ? 'active' : null }}"> <a href="{{url('records')}}" aria-expanded="false"><i class="mdi mdi-file-export"></i><span class="hide-menu">View All Records</span></a>
                </li>

                <li class="{{ Request::is('reports') ? 'active' : null }}"> <a href="{{url('reports')}}" aria-expanded="false"><i class="mdi mdi-printer"></i><span class="hide-menu">Reports</span></a>
                </li>
                
                <li class="nav-small-cap">Credentials</li>

                <li class="{{ Request::is('manage-users') ? 'active' : null }}">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);">
                        <i class="mdi mdi-account-key"></i><span class="hide-menu">User Management</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('manage-users/create')}}">Add User</a></li>
                        <li><a href="{{url('manage-users')}}">View All User</a></li>
                    </ul>
                </li>

                
            </ul>
        </nav>
    </div>
</aside>
